using ADPP.Po.Algoritmes.Dijkstras.Dijkstras;
using ADPP.Po.Algoritmes.Dijkstras.Graph;
using System;
using System.Collections.Generic;
using Xunit;

namespace ADPP.Po.Algoritmes.Dijkstras.Tests
{
    public class GraphTests
    {
        [Fact]
        public void Graph_Should_Be_Empty_By_Default()
        {
            Graph<string> graph = new Graph<string>();
            Assert.True(graph.Vertices.Count == 0);
        }

        [Fact]
        public void Adding_A_Vertex_Should_Actually_Add_It_To_The_Graph()
        {
            Graph<string> graph = new Graph<string>();
            UnweightedVertex<string> V0 = new UnweightedVertex<string>("V0");
            graph.AddVertex(V0);
            Assert.True(graph.Vertices[0].Equals(V0));
        }

        [Fact]
        public void Adding_An_Edge_Should_Connect_A_Vertex_To_Another()
        {
            UnweightedVertex<string> V0 = new UnweightedVertex<string>("V0");
            UnweightedVertex<string> V1 = new UnweightedVertex<string>("V1");
            V0.AddEdgeTo(V1);

            Assert.True(V0.Edges[0].Target == V1);
        }

        [Fact]
        public void UnweightedVertex_GetCost_From_Edge_Should_Return_One()
        {
            UnweightedVertex<string> V0 = new UnweightedVertex<string>("V0");
            UnweightedVertex<string> V1 = new UnweightedVertex<string>("V1");
            V0.AddEdgeTo(V1);

            Assert.True(V0.Edges[0].GetCosts() == 1);
        }

        [Fact]
        public void WeightedVertex_GetCost_From_Edge_Should_Return_Given_Costs()
        {
            int costs = 45;
            WeightedVertex<string> V0 = new WeightedVertex<string>("V0");
            WeightedVertex<string> V1 = new WeightedVertex<string>("V1");
            V0.AddEdgeTo(V1, costs);

            Assert.True(V0.Edges[0].GetCosts() == costs);
        }

        [Fact]
        public void WeightedVertex_Given_Costs_Cannot_Be_0_Or_Lower()
        {
            int costs = 0;
            WeightedVertex<string> V0 = new WeightedVertex<string>("V0");
            WeightedVertex<string> V1 = new WeightedVertex<string>("V1");
            

            Assert.Throws<IndexOutOfRangeException>(() => V0.AddEdgeTo(V1, costs));
        }

        [Fact]
        public void UnweightedGraph_Route_Is_Correct()
        {
            Graph<string> graph = new Graph<string>();

            UnweightedVertex<string> V0 = new UnweightedVertex<string>("V0");
            UnweightedVertex<string> V1 = new UnweightedVertex<string>("V1");
            UnweightedVertex<string> V2 = new UnweightedVertex<string>("V2");
            UnweightedVertex<string> V3 = new UnweightedVertex<string>("V3");
            UnweightedVertex<string> V4 = new UnweightedVertex<string>("V4");
            UnweightedVertex<string> V5 = new UnweightedVertex<string>("V5");
            UnweightedVertex<string> V6 = new UnweightedVertex<string>("V6");
            UnweightedVertex<string> V7 = new UnweightedVertex<string>("V7");

            V0.AddEdgeTo(V1);
            V0.AddEdgeTo(V3);
            V1.AddEdgeTo(V3);
            V1.AddEdgeTo(V4);
            V2.AddEdgeTo(V5);
            V2.AddEdgeTo(V0);
            V2.AddEdgeTo(V3);
            V3.AddEdgeTo(V2);
            V3.AddEdgeTo(V4);
            V3.AddEdgeTo(V5);
            V3.AddEdgeTo(V6);
            V4.AddEdgeTo(V6);
            V6.AddEdgeTo(V5);
            V6.AddEdgeTo(V7);

            graph.AddVertex(V0);
            graph.AddVertex(V1);
            graph.AddVertex(V2);
            graph.AddVertex(V3);
            graph.AddVertex(V4);
            graph.AddVertex(V5);
            graph.AddVertex(V6);
            graph.AddVertex(V7);

            List<string> route = graph.FindRoute(V2, V7);
            List<string> correctRoute = new List<string>() { "V2", "V3", "V6", "V7" };

            Assert.Equal(route, correctRoute);
        }

        [Fact]
        public void WeightedGraph_Route_Is_Correct()
        {
            Graph<string> graph = new Graph<string>();

            WeightedVertex<string> V0 = new WeightedVertex<string>("V0");
            WeightedVertex<string> V1 = new WeightedVertex<string>("V1");
            WeightedVertex<string> V2 = new WeightedVertex<string>("V2");
            WeightedVertex<string> V3 = new WeightedVertex<string>("V3");
            WeightedVertex<string> V4 = new WeightedVertex<string>("V4");
            WeightedVertex<string> V5 = new WeightedVertex<string>("V5");
            WeightedVertex<string> V6 = new WeightedVertex<string>("V6");
            WeightedVertex<string> V7 = new WeightedVertex<string>("V7");

            V0.AddEdgeTo(V1, 2);
            V0.AddEdgeTo(V3, 1);
            V1.AddEdgeTo(V3, 3);
            V1.AddEdgeTo(V4, 10);
            V2.AddEdgeTo(V5, 5);
            V2.AddEdgeTo(V0, 4);
            V2.AddEdgeTo(V3, 18);
            V3.AddEdgeTo(V2, 2);
            V3.AddEdgeTo(V4, 2);
            V3.AddEdgeTo(V5, 8);
            V3.AddEdgeTo(V6, 4);
            V4.AddEdgeTo(V6, 6);
            V6.AddEdgeTo(V5, 1);
            V6.AddEdgeTo(V7, 2);

            graph.AddVertex(V0);
            graph.AddVertex(V1);
            graph.AddVertex(V2);
            graph.AddVertex(V3);
            graph.AddVertex(V4);
            graph.AddVertex(V5);
            graph.AddVertex(V6);
            graph.AddVertex(V7);

            List<string> route = graph.FindRoute(V2, V7);
            List<string> correctRoute = new List<string>() { "V2", "V0", "V3", "V6", "V7" };

            Assert.Equal(route, correctRoute);
        }
    }
}
