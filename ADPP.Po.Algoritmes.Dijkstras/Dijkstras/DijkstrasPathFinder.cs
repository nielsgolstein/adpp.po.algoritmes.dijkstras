﻿using ADPP.Po.Algoritmes.Dijkstras.Graph;
using System.Collections.Generic;
using System.Linq;

namespace ADPP.Po.Algoritmes.Dijkstras.Dijkstras
{
    public static class DijkstrasPathFinder
    {
        /// <summary>
        /// Holds the extended vertecies from the graph.
        /// </summary>
        private static List<VertexExtension> ExtendedVertecies;


        public static List<Q> FindRoute<Q>(this Graph<Q> graph, Vertex<Q> from, Vertex<Q> to)
        {
            ExtendedVertecies = new List<VertexExtension>();
            graph.Vertices.ForEach(v => { ExtendedVertecies.Add(VertexExtension.Transform(v)); });

            //Search the vertex FROM
            VertexExtension ext = FindExtendedVertex(from);

            //Read recurse.
            RecurseForward(ext);

            return IterateBack<Q>(null, from, to);
        }


        /// <summary>
        /// Feeds forward in the graph using the neighbors from the current node. This method is used recursively to calculate the entire route from source to destination
        /// </summary>
        /// <param name="currentNode">The current node</param>
        private static void RecurseForward(VertexExtension currentNode)
        {
            if (currentNode == null)
                return;

            //flag the node as Visited to prevent this node from being looped.
            currentNode.Visited = true;

            //Get all Adjacent nodes end Edges and calculate the total costs.
            List<VertexExtension> destinations = new List<VertexExtension>();
            foreach (Edge adjEdge in currentNode.BaseVertexObject.Edges)
            {
                VertexExtension target = FindExtendedVertex(adjEdge.Target);

                //Summing the weight, only update if the new weight is cheaper than the already set one (default 0)
                int summedWeight = adjEdge.GetCosts() + currentNode.SumWeight;
                if (target.SumWeight == 0 || target.SumWeight > summedWeight)
                {
                    target.PreviousNode = currentNode;

                    //update the summedWeight to the previous node its weight + Edge costs.
                    target.SumWeight = summedWeight;
                }

                destinations.Add(target);
            }


            //Trigger all the found destinations except the visited ones.
            foreach (VertexExtension dNode in destinations.OrderBy(q => q.SumWeight))
            {
                if (dNode.Visited)
                    continue;

                //Loop recurrent.
                RecurseForward(dNode);
            }

        }

        /// <summary>
        /// Simply finds the extended vertex based on the default type.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        private static VertexExtension FindExtendedVertex(IVertex v)
        {
            return ExtendedVertecies.FirstOrDefault(q => q.BaseVertexObject == v);
        }

        /// <summary>
        /// Reads back the results using the results from the iteration forward.
        /// </summary>
        /// <param name="vertecies">A list which will be filled recursively</param>
        /// <param name="fromVertex">The vertex where we want our route FROM</param>
        /// <param name="toVertex">The vertex where we want our route TO</param>
        /// <returns>List of data.</returns>
        private static List<T> IterateBack<T>(LinkedList<Vertex<T>> vertecies, IVertex fromVertex, IVertex toVertex)
        {
            //Get the vertecies with the extended data.
            VertexExtension from = FindExtendedVertex(fromVertex);
            VertexExtension to = FindExtendedVertex(toVertex);

            vertecies = vertecies ?? new LinkedList<Vertex<T>>();

            if (to.PreviousNode == null)
                return vertecies.Select(q => q.Data).ToList();
            if (fromVertex == toVertex)
            {
                vertecies.AddLast((Vertex<T>) fromVertex);
                return vertecies.Select(q => q.Data).ToList();
            }

            IterateBack(vertecies, fromVertex, to.PreviousNode.BaseVertexObject);

            if (vertecies.Count != 0)
                vertecies.AddLast((Vertex<T>)toVertex);

            return vertecies.Select(q => q.Data).ToList();
        }
    }
}
