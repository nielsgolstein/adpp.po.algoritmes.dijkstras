﻿using ADPP.Po.Algoritmes.Dijkstras.Graph;
namespace ADPP.Po.Algoritmes.Dijkstras.Dijkstras
{
    public class VertexExtension
    {
        /// <summary>
        /// The node
        /// </summary>
        public IVertex BaseVertexObject { get; private set; }

        /// <summary>
        /// Total weight
        /// </summary>
        public int SumWeight { get; set; }

        /// <summary>
        /// The previous node
        /// </summary>
        public VertexExtension PreviousNode { get; set; }

        /// <summary>
        /// Indicates whenever this node is visited
        /// </summary>
        public bool Visited { get; set; }

        public VertexExtension(IVertex node)
        {
            this.BaseVertexObject = node;
        }

        public static VertexExtension Transform(IVertex node)
        {
            return new VertexExtension(node);
        }
    }
}
