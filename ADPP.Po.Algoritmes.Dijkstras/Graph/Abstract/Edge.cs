﻿namespace ADPP.Po.Algoritmes.Dijkstras.Graph
{
    public abstract class Edge
    {
        public IVertex Source { get; private set; }
        public IVertex Target { get; private set; }

        public Edge(IVertex s, IVertex t)
        {
            this.Source = s;
            this.Target = t;
        }

        /// <summary>
        /// The method which returns the cost to travel this edge to the other vertex. The cost method is abstract so it can be used by several implementations which will return other values.
        /// </summary>
        /// <returns>The cost as <see cref="int"/></returns>
        public abstract int GetCosts();
    }
}