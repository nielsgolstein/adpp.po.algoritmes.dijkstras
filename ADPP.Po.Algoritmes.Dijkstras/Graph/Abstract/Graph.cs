﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADPP.Po.Algoritmes.Dijkstras.Graph
{
    public class Graph<T>
    {
        /// <summary>
        /// Holds all the vertecies of this graph. The vertecies their self contains the edges.
        /// </summary>
        public List<Vertex<T>> Vertices { get; private set; }

        public Graph()
        {
            Vertices = new List<Vertex<T>>(); //Make sure list is never null.
        }

        public void AddVertex(Vertex<T> vertex)
        {
            if (vertex == null)
                throw new NullReferenceException("The provided vertex cannot be NULL.");

            Vertices.Add(vertex);
        }
    }
}
