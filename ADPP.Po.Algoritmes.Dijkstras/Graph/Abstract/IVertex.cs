﻿using System.Collections.Generic;

namespace ADPP.Po.Algoritmes.Dijkstras.Graph
{
    public interface IVertex
    {
        List<Edge> Edges { get; }
    }
}