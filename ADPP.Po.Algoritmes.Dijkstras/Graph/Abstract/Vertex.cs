﻿using System.Collections.Generic;

namespace ADPP.Po.Algoritmes.Dijkstras.Graph
{
    public abstract class Vertex<T> : IVertex
    {
        /// <summary>
        /// Holds all the edges to the adjacent relations.
        /// </summary>
        public List<Edge> Edges { get; private set; }

        public T Data { get; private set; }

        public Vertex(T data)
        {
            Edges = new List<Edge>();
            Data = data;
        }

        /// <summary>
        /// Adds an edge to this specific Vertex with a relation to the adjacent vertex given.
        /// </summary>
        /// <param name="adjVertex">The adjacent vertex where this vertex has a travel option to.</param>
        public abstract void AddEdgeTo(Vertex<T> adjVertex);
    }
}