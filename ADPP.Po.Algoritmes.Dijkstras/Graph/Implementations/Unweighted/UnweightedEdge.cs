﻿namespace ADPP.Po.Algoritmes.Dijkstras.Graph
{
    public class UnweightedEdge : Edge
    {
        public UnweightedEdge(IVertex s, IVertex t) : base(s, t) { }

        public override int GetCosts()
        {
            return 1; //Return static value 1.
        }
    }
}