﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADPP.Po.Algoritmes.Dijkstras.Graph
{
    public class UnweightedVertex<T> : Vertex<T>
    {
        public UnweightedVertex(T data) : base(data)
        {

        }

        public override void AddEdgeTo(Vertex<T> adjVertex)
        {
            UnweightedEdge edge = new UnweightedEdge(this, adjVertex);

            Edges.Add(edge);
        }
    }
}
