﻿using System;
namespace ADPP.Po.Algoritmes.Dijkstras.Graph
{
    public class WeightedEdge : Edge
    {
        public int Weight { get; private set; }

        public WeightedEdge(IVertex s, IVertex t, int weight) : base(s, t) {
            this.Weight = weight;
        }

        public override int GetCosts()
        {
            return Weight; //Return static value 1.
        }
    }
}