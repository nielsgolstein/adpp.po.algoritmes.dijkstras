﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ADPP.Po.Algoritmes.Dijkstras.Graph
{
    public class WeightedVertex<T> : Vertex<T>
    {
        public WeightedVertex(T data) : base(data)
        {

        }

        public override void AddEdgeTo(Vertex<T> adjVertex)
        {
            WeightedEdge edge = new WeightedEdge(this, adjVertex, Int32.MaxValue);
            Edges.Add(edge);
        }

        public void AddEdgeTo(Vertex<T> adjVertex, int weight)
        {
            if (weight <= 0)
                throw new IndexOutOfRangeException("The given weight cannot be 0 or smaller.");

            WeightedEdge edge = new WeightedEdge(this, adjVertex, weight);
            Edges.Add(edge);
        }
    }
}
