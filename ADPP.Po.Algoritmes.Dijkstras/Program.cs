﻿using ADPP.Po.Algoritmes.Dijkstras.Dijkstras;
using ADPP.Po.Algoritmes.Dijkstras.Graph;
using System;
using System.Collections.Generic;

namespace ADPP.Po.Algoritmes.Dijkstras
{
    class Program
    {
        static void Main(string[] args)
        {
            PerformWeighted();
            Console.WriteLine("");
            Console.WriteLine("---");
            PerformUnweighted();
            Console.ReadLine();
        }


        private static void PerformWeighted()
        {
            Graph<string> graph = new Graph<string>();

            WeightedVertex<string> V0 = new WeightedVertex<string>("V0");
            WeightedVertex<string> V1 = new WeightedVertex<string>("V1");
            WeightedVertex<string> V2 = new WeightedVertex<string>("V2");
            WeightedVertex<string> V3 = new WeightedVertex<string>("V3");
            WeightedVertex<string> V4 = new WeightedVertex<string>("V4");
            WeightedVertex<string> V5 = new WeightedVertex<string>("V5");
            WeightedVertex<string> V6 = new WeightedVertex<string>("V6");
            WeightedVertex<string> V7 = new WeightedVertex<string>("V7");

            V0.AddEdgeTo(V1, 2);
            V0.AddEdgeTo(V3, 1);
            V1.AddEdgeTo(V3, 3);
            V1.AddEdgeTo(V4, 10);
            V2.AddEdgeTo(V5, 5);
            V2.AddEdgeTo(V0, 4);
            V3.AddEdgeTo(V2, 2);
            V3.AddEdgeTo(V4, 2);
            V3.AddEdgeTo(V5, 8);
            V3.AddEdgeTo(V6, 4);
            V4.AddEdgeTo(V6, 6);
            V6.AddEdgeTo(V5, 1);
            V6.AddEdgeTo(V7, 2);

            graph.AddVertex(V0);
            graph.AddVertex(V1);
            graph.AddVertex(V2);
            graph.AddVertex(V3);
            graph.AddVertex(V4);
            graph.AddVertex(V5);
            graph.AddVertex(V6);
            graph.AddVertex(V7);

            List<string> res = graph.FindRoute(V2, V7);

            foreach (string r in res)
            {
                Console.Write($"{r}, ");
            }
        }

        private static void PerformUnweighted()
        {
            Graph<string> graph = new Graph<string>();

            UnweightedVertex<string> V0 = new UnweightedVertex<string>("V0");
            UnweightedVertex<string> V1 = new UnweightedVertex<string>("V1");
            UnweightedVertex<string> V2 = new UnweightedVertex<string>("V2");
            UnweightedVertex<string> V3 = new UnweightedVertex<string>("V3");
            UnweightedVertex<string> V4 = new UnweightedVertex<string>("V4");
            UnweightedVertex<string> V5 = new UnweightedVertex<string>("V5");
            UnweightedVertex<string> V6 = new UnweightedVertex<string>("V6");
            UnweightedVertex<string> V7 = new UnweightedVertex<string>("V7");

            V0.AddEdgeTo(V1);
            V0.AddEdgeTo(V3);
            V1.AddEdgeTo(V3);
            V1.AddEdgeTo(V4);
            V2.AddEdgeTo(V5);
            V2.AddEdgeTo(V0);
            V2.AddEdgeTo(V3);
            V3.AddEdgeTo(V2);
            V3.AddEdgeTo(V4);
            V3.AddEdgeTo(V5);
            V3.AddEdgeTo(V6);
            V4.AddEdgeTo(V6);
            V6.AddEdgeTo(V5);
            V6.AddEdgeTo(V7);

            graph.AddVertex(V0);
            graph.AddVertex(V1);
            graph.AddVertex(V2);
            graph.AddVertex(V3);
            graph.AddVertex(V4);
            graph.AddVertex(V5);
            graph.AddVertex(V6);
            graph.AddVertex(V7);

            List<string> res = graph.FindRoute(V2, V7);

            foreach (string r in res)
            {
                Console.Write($"{r}, ");
            }
        }
    }
}
